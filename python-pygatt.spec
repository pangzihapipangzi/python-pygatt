%global pypi_name pygatt
%{?python_disable_dependency_generator}

Name:           python-pygatt
Version:        4.0.5
Release:        1
Summary:        A Python Module for Bluetooth LE Generic Attribute Profile

License:        ASL 2.0
URL:            https://github.com/peplin/pygatt
Source0:        https://github.com/peplin/pygatt/archive/v%{version}/%{pypi_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-nose
BuildRequires:  python3-coverage

Requires:       bluez

%description
This Module allows reading and writing to GATT descriptors on devices such as
fitness trackers, sensors, and anything implementing standard GATT Descriptor
behavior.

pygatt wraps BlueZ's 'gatttool' command-line utility with a Pythonic API.

%package -n python3-%{pypi_name}
Summary:        %{summary}
Requires:       python3-pexpect
Requires:       python3-pyserial
Requires:       bluez
%{?python_provide:%python_provide python3-%{pypi_name}}

%description -n python3-%{pypi_name}
This Module allows reading and writing to GATT descriptors on devices such as
fitness trackers, sensors, and anything implementing standard GATT Descriptor
behavior.

pygatt wraps BlueZ's 'gatttool' command-line utility with a Pythonic API.

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%doc README.rst
%license LICENSE
%{python3_sitelib}/%{pypi_name}/
%{python3_sitelib}/*.egg-info

%changelog
* Tue Jul 6 2021 Like  <like@kylinos.cn> - 4.0.5-1
- init package

















































































